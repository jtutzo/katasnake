package com.jtutzo.katasnake.adapters.gateways;

import com.jtutzo.katasnake.domain.gateways.CurrentUserGateway;
import com.jtutzo.katasnake.domain.model.User;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

import static java.util.Optional.*;

@ApplicationScoped
public class InMemoryCurrentUserGateway implements CurrentUserGateway {
    @Override
    public Optional<User> get() {
        return of(User.builder().name("jeremy.tutzo").build());
    }
}
