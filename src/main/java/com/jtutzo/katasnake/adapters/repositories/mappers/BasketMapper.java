package com.jtutzo.katasnake.adapters.repositories.mappers;

import com.jtutzo.katasnake.adapters.repositories.entities.BasketEntity;
import com.jtutzo.katasnake.domain.model.Basket;
import com.jtutzo.katasnake.domain.model.User;
import lombok.val;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import static java.util.stream.Collectors.toSet;

@ApplicationScoped
public class BasketMapper {

    @Inject
    private GoodMapper goodMapper;

    public BasketEntity toBasketEntity(Basket basket) {
        if (basket == null) throw new RuntimeException("Basket mustn't be null !");
        val goods = basket.getGoods().stream().map(goodMapper::toGoodEntity).collect(toSet());
        return BasketEntity
                .builder()
                .name(basket.getName())
                .user(basket.getUser().getName())
                .goods(goods)
                .build();
    }

    public Basket toBasket(BasketEntity basketEntity) {
        if (basketEntity == null) return null;
        val goods = basketEntity.getGoods().stream().map(goodMapper::toGood).collect(toSet());
        val user = User.builder().name(basketEntity.getUser()).build();
        return Basket.builder().name(basketEntity.getName()).user(user).goods(goods).build();
    }
}
