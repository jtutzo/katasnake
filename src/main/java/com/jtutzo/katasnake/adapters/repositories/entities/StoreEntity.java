package com.jtutzo.katasnake.adapters.repositories.entities;

import io.quarkus.mongodb.panache.MongoEntity;
import lombok.*;

import java.util.Set;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@MongoEntity(collection = "store")
public class StoreEntity {
    private String name;
    private Set<GoodEntity> goods;
}
