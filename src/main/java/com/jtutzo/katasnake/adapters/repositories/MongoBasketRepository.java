package com.jtutzo.katasnake.adapters.repositories;

import com.jtutzo.katasnake.adapters.repositories.entities.BasketEntity;
import com.jtutzo.katasnake.adapters.repositories.mappers.BasketMapper;
import com.jtutzo.katasnake.domain.model.Basket;
import com.jtutzo.katasnake.domain.repositories.BasketRepository;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import lombok.val;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@ApplicationScoped
public class MongoBasketRepository implements BasketRepository {

    private MongoRepository repository = new MongoRepository();

    @Inject
    private BasketMapper basketMapper;

    @Override
    public Optional<Basket> find(String name) {
        val basketEntity = repository.find("name = ?1", name).firstResult();
        return ofNullable(basketMapper.toBasket(basketEntity));
    }

    @Override
    public void save(Basket basket) {
        val basketEntity = basketMapper.toBasketEntity(basket);
        this.delete(basketEntity.getName());
        repository.persist(basketEntity);
    }

    @Override
    public Set<Basket> findAll() {
        return repository.findAll()
                .stream()
                .map(basketMapper::toBasket)
                .collect(Collectors.toSet());
    }

    @Override
    public void delete(String name) {
        repository.delete("name = ?1", name);
    }

    private static class MongoRepository implements PanacheMongoRepository<BasketEntity> {
    }
}
