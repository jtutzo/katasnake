package com.jtutzo.katasnake.adapters.repositories.mappers;

import com.jtutzo.katasnake.adapters.repositories.entities.StoreEntity;
import com.jtutzo.katasnake.domain.model.Store;
import lombok.val;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import static java.util.stream.Collectors.toSet;

@ApplicationScoped
public class StoreMapper {

    @Inject
    private GoodMapper goodMapper;

    public StoreEntity toStoreEntity(Store store) {
        if (store == null) throw new RuntimeException("Store mustn't be null !");
        val goods = store.getGoods().stream().map(goodMapper::toGoodEntity).collect(toSet());
        return StoreEntity
                .builder()
                .name(store.getName())
                .goods(goods)
                .build();
    }

    public Store toStore(StoreEntity storeEntity) {
        if (storeEntity == null) return null;
        val goods = storeEntity.getGoods().stream().map(goodMapper::toGood).collect(toSet());
        return Store.builder().name(storeEntity.getName()).goods(goods).build();
    }

}
