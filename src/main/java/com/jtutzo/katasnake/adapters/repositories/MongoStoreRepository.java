package com.jtutzo.katasnake.adapters.repositories;

import com.jtutzo.katasnake.adapters.repositories.entities.StoreEntity;
import com.jtutzo.katasnake.adapters.repositories.mappers.StoreMapper;
import com.jtutzo.katasnake.domain.model.Store;
import com.jtutzo.katasnake.domain.repositories.StoreRepository;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import lombok.val;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@ApplicationScoped
public class MongoStoreRepository implements StoreRepository {

    private MongoRepository repository = new MongoRepository();

    @Inject
    private StoreMapper storeMapper;

    @Override
    public Optional<Store> find(String name) {
        val result = repository.find("name = ?1", name).firstResult();
        return ofNullable(storeMapper.toStore(result));
    }

    @Override
    public void save(Store store) {
        val storeEntity = storeMapper.toStoreEntity(store);
        this.delete(storeEntity.getName());
        repository.persist(storeEntity);
    }

    @Override
    public Set<Store> findAll() {
        return repository.findAll()
                .stream()
                .map(storeMapper::toStore)
                .collect(Collectors.toSet());
    }

    @Override
    public void delete(String name) {
        repository.delete("name = ?1", name);
    }

    private static class MongoRepository implements PanacheMongoRepository<StoreEntity> {
    }
}
