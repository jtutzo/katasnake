package com.jtutzo.katasnake.adapters.repositories.mappers;

import com.jtutzo.katasnake.adapters.repositories.entities.GoodEntity;
import com.jtutzo.katasnake.domain.factories.GoodFactory;
import com.jtutzo.katasnake.domain.model.Good;
import com.jtutzo.katasnake.domain.model.StockIdentifier;
import lombok.val;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class GoodMapper {

    @Inject
    private GoodFactory goodFactory;

    public GoodEntity toGoodEntity(Good good) {
        if (good == null) throw new RuntimeException("Good mustn't be null !");
        val maybeOrigin = good.getMaybeOrigin().orElse(StockIdentifier.buildDefault()).getName();
        return GoodEntity.builder()
                .product(good.getProduct().getName())
                .quantity(good.getQuantity().getValue())
                .maybeOrigin(maybeOrigin)
                .build();
    }

    public Good toGood(GoodEntity goodEntity) {
        if (goodEntity == null) return null;
        return goodFactory.build(
                goodEntity.getProduct(),
                goodEntity.getQuantity(),
                goodEntity.getMaybeOrigin());
    }
}
