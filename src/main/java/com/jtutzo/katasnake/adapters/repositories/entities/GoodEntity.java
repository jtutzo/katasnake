package com.jtutzo.katasnake.adapters.repositories.entities;

import lombok.*;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GoodEntity {
    private String product;
    private Integer quantity;
    private String maybeOrigin;
}
