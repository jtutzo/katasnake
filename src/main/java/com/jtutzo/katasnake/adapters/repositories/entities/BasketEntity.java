package com.jtutzo.katasnake.adapters.repositories.entities;

import io.quarkus.mongodb.panache.MongoEntity;
import lombok.*;

import java.util.Set;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@MongoEntity(collection = "basket")
public class BasketEntity {
    private String name;
    private String user;
    private Set<GoodEntity> goods;
}
