package com.jtutzo.katasnake.resources;

import com.jtutzo.katasnake.domain.factories.GoodFactory;
import com.jtutzo.katasnake.domain.model.Store;
import com.jtutzo.katasnake.domain.services.StoreService;
import com.jtutzo.katasnake.resources.dto.AddGood;
import com.jtutzo.katasnake.resources.dto.CreateStore;
import lombok.val;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;
import java.util.Set;

import static javax.ws.rs.core.Response.ok;
import static javax.ws.rs.core.Response.status;

@Path("/stores")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StoreResource {

    @Inject
    private StoreService storeService;

    @Inject
    private GoodFactory goodFactory;

    @GET
    public Response findAll() {
        Set<Store> stores = storeService.findAll();
        return ok(stores).build();
    }

    @GET
    @Path("/{name}")
    public Response find(@PathParam("name") String name) {
        Optional<Store> maybeStore = storeService.find(name);
        if (maybeStore.isPresent())
            return ok(maybeStore.get()).build();
        return status(Response.Status.NOT_FOUND).build();
    }

    @POST
    public Response create(@Valid CreateStore createStore) {
        Store store = storeService.create(createStore.getName());
        return Response.ok(store).status(Response.Status.CREATED).build();
    }

    @PUT
    @Path("/{name}/add")
    public Response addGood(@PathParam("name") String name, AddGood addGood) {
        val good = goodFactory.build(addGood.getProduct(), addGood.getQuantity(), addGood.getOrigin());
        storeService.addGood(name, good);
        return ok().build();
    }

    @DELETE
    @Path("/{name}")
    public Response delete(@PathParam("name") String name) {
        storeService.delete(name);
        return ok().build();
    }
}
