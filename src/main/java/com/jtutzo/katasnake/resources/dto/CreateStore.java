package com.jtutzo.katasnake.resources.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateStore {
    @NotBlank(message = "Name mustn't be bull")
    String name;
}
