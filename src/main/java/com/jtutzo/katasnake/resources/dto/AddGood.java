package com.jtutzo.katasnake.resources.dto;


import lombok.*;

import javax.validation.constraints.NotBlank;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AddGood {
    @NotBlank(message = "Name mustn't be bull")
    String product;
    @NotBlank(message = "Name mustn't be bull")
    Integer quantity;
    String origin;
}
