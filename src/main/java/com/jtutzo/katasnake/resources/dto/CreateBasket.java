package com.jtutzo.katasnake.resources.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CreateBasket {
    @NotBlank(message = "Name mustn't be bull")
    String name;
}
