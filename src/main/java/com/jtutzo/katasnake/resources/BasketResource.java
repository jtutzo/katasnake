package com.jtutzo.katasnake.resources;

import com.jtutzo.katasnake.domain.factories.GoodFactory;
import com.jtutzo.katasnake.domain.model.Basket;
import com.jtutzo.katasnake.domain.services.BasketService;
import com.jtutzo.katasnake.resources.dto.AddGood;
import com.jtutzo.katasnake.resources.dto.CreateBasket;
import lombok.val;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;
import java.util.Set;

import static javax.ws.rs.core.Response.*;

@Path("/baskets")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BasketResource {

    @Inject
    private BasketService basketService;

    @Inject
    private GoodFactory goodFactory;

    @GET
    public Response findAll() {
        Set<Basket> baskets = basketService.findAll();
        return ok(baskets).build();
    }

    @GET
    @Path("/{name}")
    public Response find(@PathParam("name") String name) {
        Optional<Basket> maybeBasket = basketService.find(name);
        if (maybeBasket.isPresent())
            return ok(maybeBasket.get()).build();
        return status(Status.NOT_FOUND).build();
    }

    @POST
    public Response create(@Valid CreateBasket createBasket) {
        Basket basket = basketService.create(createBasket.getName());
        return ok(basket).status(Status.CREATED).build();
    }

    @PUT
    @Path("/{name}/add")
    public Response addGood(@PathParam("name") String name, AddGood addGood) {
        val good = goodFactory.build(addGood.getProduct(), addGood.getQuantity(), addGood.getOrigin());
        basketService.addGood(name, good);
        return ok().build();
    }

    @DELETE
    @Path("/{name}")
    public Response delete(@PathParam("name") String name) {
        basketService.delete(name);
        return ok().build();
    }
}
