package com.jtutzo.katasnake.domain.factories;

import com.jtutzo.katasnake.domain.model.Good;
import com.jtutzo.katasnake.domain.model.Product;
import com.jtutzo.katasnake.domain.model.Quantity;
import com.jtutzo.katasnake.domain.model.StockIdentifier;
import lombok.val;

import javax.enterprise.context.ApplicationScoped;

import static java.util.Optional.ofNullable;

@ApplicationScoped
public class GoodFactory {
    public Good build(String productName, Integer quantityValue, String originName) {
        val product = Product.builder().name(productName).build();
        val quantity = Quantity.builder().value(quantityValue).build();
        val origin = originName != null ? StockIdentifier.builder().name(originName).build() : null;
        return Good
                .builder()
                .product(product)
                .quantity(quantity)
                .maybeOrigin(ofNullable(origin))
                .build();
    }
}
