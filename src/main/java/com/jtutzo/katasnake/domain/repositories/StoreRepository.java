package com.jtutzo.katasnake.domain.repositories;

import com.jtutzo.katasnake.domain.model.Store;

import java.util.Optional;
import java.util.Set;

public interface StoreRepository {
    Optional<Store> find(String name);

    void save(Store store);

    Set<Store> findAll();

    void delete(String name);
}
