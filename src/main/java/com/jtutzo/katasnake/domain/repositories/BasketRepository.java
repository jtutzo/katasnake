package com.jtutzo.katasnake.domain.repositories;

import com.jtutzo.katasnake.domain.model.Basket;

import java.util.Optional;
import java.util.Set;

public interface BasketRepository {
    Optional<Basket> find(String name);

    void save(Basket basket);

    Set<Basket> findAll();

    void delete(String name);
}
