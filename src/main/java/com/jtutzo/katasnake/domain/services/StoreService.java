package com.jtutzo.katasnake.domain.services;

import com.jtutzo.katasnake.domain.exceptions.NotFoundEntityException;
import com.jtutzo.katasnake.domain.model.Good;
import com.jtutzo.katasnake.domain.model.Store;
import com.jtutzo.katasnake.domain.repositories.StoreRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.val;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@ApplicationScoped
public class StoreService {

    @Inject
    private StoreRepository storeRepository;

    public Optional<Store> find(String name) {
        return storeRepository.find(name);
    }

    public Store create(String storeName) {
        Store store = Store.builder().name(storeName).build();
        storeRepository.save(store);
        return store;
    }

    public Set<Store> findAll() {
        return storeRepository.findAll();
    }

    public void delete(String name) {
        storeRepository.delete(name);
    }

    public void addGood(String storeName, Good good) {

        val store = find(storeName).orElseThrow(() -> {
            throw new NotFoundEntityException("Store " + storeName + "not found !");
        });

        store.add(good);
        storeRepository.save(store);
    }

    public void save(Store store) {
        storeRepository.save(store);
    }
}
