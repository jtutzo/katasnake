package com.jtutzo.katasnake.domain.services;

import com.jtutzo.katasnake.domain.exceptions.NotFoundEntityException;
import com.jtutzo.katasnake.domain.model.Basket;
import com.jtutzo.katasnake.domain.model.Good;
import com.jtutzo.katasnake.domain.model.StockIdentifier;
import com.jtutzo.katasnake.domain.model.usecases.ExchangeGood;
import com.jtutzo.katasnake.domain.repositories.BasketRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.val;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@ApplicationScoped
public class BasketService {

    @Inject
    private BasketRepository basketRepository;

    @Inject
    private UserService userService;

    @Inject
    private StoreService storeService;

    private ExchangeGood exchangeGood = new ExchangeGood();

    public Optional<Basket> find(String name) {
        return basketRepository.find(name);
    }

    public Basket create(String name) {

        val user = userService.getCurrentUser().orElseThrow(() -> {
            throw new NotFoundEntityException("Current user not found !");
        });

        Basket basket = Basket.builder().name(name).user(user).build();
        basketRepository.save(basket);

        return basket;
    }

    public void addGood(String basketName, Good good) {

        val user = userService.getCurrentUser().orElseThrow(() -> {
            throw new NotFoundEntityException("Current user not found !");
        });

        val stockIdentifier = good.getMaybeOrigin().orElse(StockIdentifier.buildDefault());
        val store = storeService.find(stockIdentifier.getName()).orElseThrow(() -> {
            throw new NotFoundEntityException("Store " + stockIdentifier.getName() + "not found !");
        });

        val basket = find(basketName).orElseThrow(() -> {
            throw new NotFoundEntityException("Basket " + basketName + "not found !");
        });

        exchangeGood.fromStoreToBasket(user, store, basket, good);

        basketRepository.save(basket);
        storeService.save(store);
    }


    public Set<Basket> findAll() {
        return basketRepository.findAll();
    }

    public void delete(String name) {
        basketRepository.delete(name);
    }
}
