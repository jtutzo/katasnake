package com.jtutzo.katasnake.domain.services;

import com.jtutzo.katasnake.domain.gateways.CurrentUserGateway;
import com.jtutzo.katasnake.domain.model.User;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@ApplicationScoped
public class UserService {

    @Inject
    private CurrentUserGateway currentUserGateway;

    public Optional<User> getCurrentUser() {
        return currentUserGateway.get();
    }
}
