package com.jtutzo.katasnake.domain.model;

import lombok.*;


import java.util.Optional;

import static java.util.Optional.empty;

@Data
@Builder
@Getter
public class Good {
    private Product product;
    @Builder.Default
    private Optional<StockIdentifier> maybeOrigin = empty();
    @Builder.Default
    private Quantity quantity = Quantity.builder().build();

    Boolean sameProduct(Product product) {
        return this.product.getName().equals(product.getName());
    }

    Good reduce(Quantity quantity) {
        return Good.builder()
                .product(product)
                .maybeOrigin(maybeOrigin)
                .quantity(this.quantity.subtract(quantity))
                .build();
    }

    Good increase(Quantity quantity) {
        return Good.builder()
                .product(product)
                .maybeOrigin(maybeOrigin)
                .quantity(this.quantity.plus(quantity))
                .build();
    }

    Boolean hasNoQuantity() {
        return quantity.isNull();
    }

    Boolean hasNotEnoughQuantity(Quantity quantity) {
        return this.quantity.isSmaller(quantity);
    }

    Boolean isEquals(Good good) {
        return this.sameOrigin(good.getMaybeOrigin()) && this.sameProduct(good.getProduct());
    }

    Boolean sameOrigin(Optional<StockIdentifier> maybeOrigin) {
        return this.maybeOrigin.equals(maybeOrigin);
    }

    Good returnWith(StockIdentifier origin) {
        return Good.builder()
                .maybeOrigin(Optional.ofNullable(origin))
                .product(this.product)
                .quantity(this.quantity)
                .build();
    }
}
