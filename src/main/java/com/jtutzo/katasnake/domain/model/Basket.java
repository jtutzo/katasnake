package com.jtutzo.katasnake.domain.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.val;

import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Getter
@SuperBuilder
public class Basket extends Stock {
    private User user;

    public void add(Good good, Store store) {
        add(good.returnWith(store));
    }

    public void remove(Good good, Store store) {
        if (!hasSameOrigin(good.getProduct(), store))
            throw new RuntimeException("L'origine du bien est incorecte");
        remove(good);
    }

    public Boolean belongTo(User user) {
        return this.user.getName().equals(user.getName());
    }

    private Boolean hasSameOrigin(Product product, Store store) {
        val goods = this.getGoods().stream()
                .filter(it -> it.sameProduct(product))
                .collect(Collectors.toList());
        if (goods.isEmpty()) {
            return Boolean.TRUE;
        }
        return goods.stream().anyMatch(it -> it.sameOrigin(ofNullable(store)));
    }
}
