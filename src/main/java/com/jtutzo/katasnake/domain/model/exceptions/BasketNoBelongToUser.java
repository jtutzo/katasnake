package com.jtutzo.katasnake.domain.model.exceptions;

public class BasketNoBelongToUser extends RuntimeException {

    public  BasketNoBelongToUser() {
        super("Vous pouvez modifier seulement vos paniers !");
    }
}
