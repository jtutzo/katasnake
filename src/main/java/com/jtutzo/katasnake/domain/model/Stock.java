package com.jtutzo.katasnake.domain.model;

import com.jtutzo.katasnake.domain.model.exceptions.ProductInsufficientAvailableQuantityException;
import com.jtutzo.katasnake.domain.model.exceptions.ProductNotAvailableException;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptySet;

@Getter
@SuperBuilder
public class Stock extends StockIdentifier {
    @Builder.Default
    private Set<Good> goods = emptySet();

    public void add(Good good) {
        val maybeGood = goods.stream().filter(it -> it.isEquals(good)).findFirst();
        val goodToUpdate = maybeGood.orElse(Good.builder().product(good.getProduct()).maybeOrigin(good.getMaybeOrigin()).build());
        this.save(goodToUpdate.increase(good.getQuantity()));
    }

    public void remove(Good good) {
        val goodFound = get(good);
        if (goodFound.hasNoQuantity())
            throw new ProductNotAvailableException(good.getProduct());
        if (goodFound.hasNotEnoughQuantity(good.getQuantity()))
            throw new ProductInsufficientAvailableQuantityException(good.getProduct());
        this.save(goodFound.reduce(good.getQuantity()));
    }

    private Good get(Good good) {
        return goods.stream()
                .filter(it -> it.sameProduct(good.getProduct()))
                .findFirst()
                .orElse(Good.builder().product(good.getProduct()).build());
    }

    private void save(Good good) {
        goods = goods.stream()
                .filter(it -> !it.isEquals(good))
                .collect(Collectors.toSet());
        goods.add(good);
    }
}
