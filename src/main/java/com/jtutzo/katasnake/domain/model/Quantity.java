package com.jtutzo.katasnake.domain.model;

import lombok.*;

import static java.lang.Math.*;

@Data
@Builder
@Getter
public class Quantity {
    @Builder.Default
    private Integer value = 0;

    public Quantity plus(Quantity quantity) {
        return Quantity.builder().value(addExact(this.value, quantity.getValue())).build();
    }

    Quantity subtract(Quantity quantity) {
        return Quantity.builder().value(subtractExact(this.value, quantity.getValue())).build();
    }

    Boolean isNull() {
        return value.equals(0);
    }

    Boolean isSmaller(Quantity quantity) {
        return this.value < quantity.getValue();
    }
}
