package com.jtutzo.katasnake.domain.model.usecases;

import com.jtutzo.katasnake.domain.model.Basket;
import com.jtutzo.katasnake.domain.model.Good;
import com.jtutzo.katasnake.domain.model.Store;
import com.jtutzo.katasnake.domain.model.User;
import com.jtutzo.katasnake.domain.model.exceptions.BasketNoBelongToUser;

public class ExchangeGood {
    public void fromStoreToBasket(User user, Store store, Basket basket, Good good) {
        if (!basket.belongTo(user))
            throw new BasketNoBelongToUser();
        store.remove(good);
        basket.add(good, store);
    }

    public void fromBasketToStore(User user, Store store, Basket basket, Good good) {
        if (!basket.belongTo(user))
            throw new BasketNoBelongToUser();
        basket.remove(good, store);
        store.add(good);
    }
}
