package com.jtutzo.katasnake.domain.model;

import lombok.*;

@Data
@Builder
@Getter
public class Product {
    private String name;
}
