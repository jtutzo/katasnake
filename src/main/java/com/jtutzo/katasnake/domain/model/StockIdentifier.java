package com.jtutzo.katasnake.domain.model;

import lombok.Data;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Data
@Getter
@SuperBuilder
public class StockIdentifier {
    private String name;

    public static StockIdentifier buildDefault() {
        return StockIdentifier.builder().build();
    }
}
