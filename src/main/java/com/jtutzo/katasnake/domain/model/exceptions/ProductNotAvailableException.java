package com.jtutzo.katasnake.domain.model.exceptions;

import com.jtutzo.katasnake.domain.model.Product;
import com.jtutzo.katasnake.domain.model.Store;

public class ProductNotAvailableException extends RuntimeException {

    public ProductNotAvailableException(Product product) {
        super("Le produit " + product.getName() + " n'est pas disponible !");
    }
}
