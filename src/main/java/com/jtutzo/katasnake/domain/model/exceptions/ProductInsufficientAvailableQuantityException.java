package com.jtutzo.katasnake.domain.model.exceptions;

import com.jtutzo.katasnake.domain.model.Product;

public class ProductInsufficientAvailableQuantityException extends RuntimeException {
    public ProductInsufficientAvailableQuantityException(Product product) {
        super("Quantité disponible insuffisante pour le produit " + product.getName() + " !");
    }
}
