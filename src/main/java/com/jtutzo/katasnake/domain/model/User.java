package com.jtutzo.katasnake.domain.model;

import lombok.*;

@Data
@Builder
@Getter
public class User {
    private String name;
}
