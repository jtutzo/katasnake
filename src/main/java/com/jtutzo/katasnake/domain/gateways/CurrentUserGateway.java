package com.jtutzo.katasnake.domain.gateways;

import com.jtutzo.katasnake.domain.model.User;

import java.util.Optional;

public interface CurrentUserGateway {
    Optional<User> get();
}
