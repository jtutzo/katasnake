package com.jtutzo.katasnake.adapters.repositories;

import com.jtutzo.katasnake.domain.model.Store;
import com.jtutzo.katasnake.domain.repositories.StoreRepository;
import io.quarkus.test.Mock;

import javax.enterprise.context.ApplicationScoped;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Mock
@ApplicationScoped
public class InMemoryStoreRepository implements StoreRepository {

    private Set<Store> stores = new LinkedHashSet<>();

    @Override
    public Optional<Store> find(String name) {
        return stores.stream().filter(it -> it.getName().equals(name)).findFirst();
    }

    @Override
    public void save(Store store) {
        this.delete(store.getName());
        stores.add(store);
    }

    @Override
    public Set<Store> findAll() {
        return stores;
    }

    @Override
    public void delete(String name) {
        stores = stores.stream()
                .filter(it -> !it.getName().equals(name))
                .collect(Collectors.toSet());
    }
}
