package com.jtutzo.katasnake.adapters.repositories;

import com.jtutzo.katasnake.domain.model.Basket;
import com.jtutzo.katasnake.domain.repositories.BasketRepository;
import io.quarkus.test.Mock;

import javax.enterprise.context.ApplicationScoped;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Mock
@ApplicationScoped
public class InMemoryBasketRepository implements BasketRepository {

    private Set<Basket> baskets = new LinkedHashSet<>();

    @Override
    public Optional<Basket> find(String name) {
        return baskets.stream().filter(it -> it.getName().equals(name)).findFirst();
    }

    @Override
    public void save(Basket basket) {
        this.delete(basket.getName());
        baskets.add(basket);
    }

    @Override
    public Set<Basket> findAll() {
        return baskets;
    }

    @Override
    public void delete(String name) {
        baskets = baskets.stream()
                .filter(it -> !it.getName().equals(name))
                .collect(Collectors.toSet());
    }
}
