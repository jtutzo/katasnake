package com.jtutzo.katasnake.domain.factories;

import com.jtutzo.katasnake.domain.model.Good;
import com.jtutzo.katasnake.domain.model.Product;
import com.jtutzo.katasnake.domain.model.Quantity;
import com.jtutzo.katasnake.domain.model.StockIdentifier;
import lombok.val;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static java.util.Optional.*;
import static org.junit.jupiter.api.Assertions.*;

class GoodFactoryTest {

    private GoodFactory goodFactory;

    private Product product = Product.builder().name("T-shirt").build();

    private Quantity quantity = Quantity.builder().value(1).build();

    private StockIdentifier origin = StockIdentifier.builder().name("origin").build();

    @BeforeEach
    public void setUp() {
        goodFactory = new GoodFactory();
    }

    @Test
    @DisplayName("Should factory good")
    public void shouldFactoryGood() {
        // Given
        val expected = Good.builder().product(product).quantity(quantity).maybeOrigin(of(origin)).build();

        // When
        val good = goodFactory.build(product.getName(), quantity.getValue(), origin.getName());

        // Then
        Assertions.assertThat(good).isEqualTo(expected);
    }

    @Test
    @DisplayName("Should factory good without origin")
    public void shouldFactoryGoodWithoutOrigin() {
        // Given
        val expected = Good.builder().product(product).quantity(quantity).maybeOrigin(empty()).build();

        // When
        val good = goodFactory.build(product.getName(), quantity.getValue(), null);

        // Then
        Assertions.assertThat(good).isEqualTo(expected);
    }
}
