package com.jtutzo.katasnake.domain.model.features;

import com.jtutzo.katasnake.domain.model.*;
import com.jtutzo.katasnake.domain.model.usecases.ExchangeGood;
import com.jtutzo.katasnake.domain.repositories.BasketRepository;
import com.jtutzo.katasnake.domain.repositories.StoreRepository;
import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Quand;
import lombok.val;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class ExchangeGoodSteps {

    private StoreRepository storeRepository;
    private BasketRepository basketRepository;

    private ExchangeGood exchangeGood;
    private CucumberState cucumberState;

    public ExchangeGoodSteps(StoreRepository storeRepository, BasketRepository basketRepository, ExchangeGood exchangeGood, CucumberState cucumberState) {
        this.storeRepository = storeRepository;
        this.basketRepository = basketRepository;
        this.exchangeGood = exchangeGood;
        this.cucumberState = cucumberState;
    }

    @Quand("^\"([^\"]*)\" ajoute \"([^\"]*)\" \"([^\"]*)\" au panier \"([^\"]*)\" de provenance de \"([^\"]*)\"$")
    public void ajouteAuPanierDeProvenance(String userName, String quantityValue, String productName, String basketName, String storeName) {
        val user = User.builder().name(userName).build();
        val quantity = Quantity.builder().value(Integer.parseInt(quantityValue)).build();
        val product = Product.builder().name(productName).build();
        val good = Good.builder().product(product).quantity(quantity).build();

        val maybeStore = storeRepository.find(storeName);
        assertThat(maybeStore).isNotEmpty();

        val maybeBasket = basketRepository.find(basketName);
        assertThat(maybeBasket).isNotEmpty();

        cucumberState.exception = catchThrowable(() -> exchangeGood.fromStoreToBasket(user, maybeStore.get(), maybeBasket.get(), good));
    }

    @Alors("^l'erreur \"([^\"]*)\" est levée$")
    public void lErreurEstLevée(String message) throws Throwable {
        assertThat(cucumberState.exception).hasMessage(message);
    }

    @Quand("^\"([^\"]*)\" supprime \"([^\"]*)\" \"([^\"]*)\" du panier \"([^\"]*)\" de provenance de \"([^\"]*)\"$")
    public void supprimeDuPanierAuMagasin(String userName, String quantityValue, String productName, String basketName, String storeName) throws Throwable {
        val user = User.builder().name(userName).build();
        val quantity = Quantity.builder().value(Integer.parseInt(quantityValue)).build();
        val product = Product.builder().name(productName).build();
        val good = Good.builder().product(product).quantity(quantity).build();

        val maybeStore = storeRepository.find(storeName);
        assertThat(maybeStore).isNotEmpty();

        val maybeBasket = basketRepository.find(basketName);
        assertThat(maybeBasket).isNotEmpty();

        cucumberState.exception = catchThrowable(() -> exchangeGood.fromBasketToStore(user, maybeStore.get(), maybeBasket.get(), good));
    }

    @Alors("^Aucune erreur est levée$")
    public void aucuneErreurEstLevée() {
        assertThat(cucumberState.exception).isNull();
    }

}
