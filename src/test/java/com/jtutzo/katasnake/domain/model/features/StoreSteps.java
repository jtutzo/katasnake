package com.jtutzo.katasnake.domain.model.features;

import com.jtutzo.katasnake.domain.model.*;
import com.jtutzo.katasnake.domain.repositories.StoreRepository;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.fr.Et;
import cucumber.api.java.fr.Lorsque;
import cucumber.api.java.fr.Quand;
import lombok.val;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class StoreSteps {

    private StoreRepository storeRepository;
    private CucumberState cucumberState;

    public StoreSteps(StoreRepository storeRepository, CucumberState cucumberState) {
        this.storeRepository = storeRepository;
        this.cucumberState = cucumberState;
    }

    @Lorsque("^des magasins existent:$")
    public void desMagasinsExistent(DataTable dataTable) {
        List<Map<String, String>> dataMaps = dataTable.asMaps(String.class, String.class);
        dataMaps.forEach(dataMap -> {
            Set<Good> goods = new LinkedHashSet<>();
            if (!dataMap.get("quantities").equals("")) {
                List<String> quantities = Arrays.stream(dataMap.get("quantities").split(",")).collect(Collectors.toList());
                List<String> products = Arrays.stream(dataMap.get("products").split(",")).collect(Collectors.toList());
                goods = IntStream.range(0, quantities.size()).mapToObj(index -> {
                    Product product = Product.builder().name(products.get(index)).build();
                    Quantity quantity = Quantity.builder().value(Integer.parseInt(quantities.get(index))).build();
                    return Good.builder().product(product).quantity(quantity).build();
                }).collect(Collectors.toSet());
            }
            Store store = Store.builder().name(dataMap.get("magasin")).goods(goods).build();
            storeRepository.save(store);
        });
    }

    @Et("^le magasin \"([^\"]*)\" contient \"([^\"]*)\" \"([^\"]*)\"$")
    public void leMagasinA(String storeName, String quantityValue, String productName) {
        Optional<Store> maybeStore = storeRepository.find(storeName);
        assertThat(maybeStore).isNotEmpty();

        Good good = maybeStore.get()
                .getGoods()
                .stream()
                .filter(it -> it.getProduct().getName().equals(productName))
                .findFirst().orElse(Good.builder().build());

        assertThat(good.getQuantity().getValue()).isEqualTo(Integer.parseInt(quantityValue));
    }

    @Quand("^l'\"([^\"]*)\" ajoute \"([^\"]*)\" \"([^\"]*)\" dans le \"([^\"]*)\"$")
    public void lAjouteDansLe(String admin, String quantityValue, String productName, String storeName) {
        val quantity = Quantity.builder().value(Integer.parseInt(quantityValue)).build();
        val product = Product.builder().name(productName).build();
        val good = Good.builder().product(product).quantity(quantity).build();

        val maybeStore = storeRepository.find(storeName);
        assertThat(maybeStore).isNotEmpty();

        cucumberState.exception = catchThrowable(() -> maybeStore.get().add(good));
    }
}
