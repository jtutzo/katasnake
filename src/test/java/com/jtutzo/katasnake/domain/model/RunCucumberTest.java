package com.jtutzo.katasnake.domain.model;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:FeaturesReport"},
        features = "src/test/resources/features",
        glue = "com.jtutzo.katasnake.domain.model.features"
)
public class RunCucumberTest {
}
