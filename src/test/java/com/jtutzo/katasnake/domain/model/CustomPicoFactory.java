package com.jtutzo.katasnake.domain.model;

import com.jtutzo.katasnake.adapters.repositories.InMemoryBasketRepository;
import com.jtutzo.katasnake.adapters.repositories.InMemoryStoreRepository;
import com.jtutzo.katasnake.domain.model.usecases.ExchangeGood;
import cucumber.runtime.java.picocontainer.PicoFactory;

public class CustomPicoFactory extends PicoFactory {

    public CustomPicoFactory() {
        addClass(InMemoryBasketRepository.class);
        addClass(InMemoryStoreRepository.class);
        addClass(ExchangeGood.class);
        addClass(CucumberState.class);
    }
}
