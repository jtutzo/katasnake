package com.jtutzo.katasnake.domain.model.features;

import com.jtutzo.katasnake.domain.model.*;
import com.jtutzo.katasnake.domain.repositories.BasketRepository;
import com.jtutzo.katasnake.domain.repositories.StoreRepository;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Et;
import cucumber.api.java.fr.Lorsque;
import lombok.val;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Optional.ofNullable;
import static org.assertj.core.api.Assertions.assertThat;

public class BasketSteps {

    private BasketRepository basketRepository;
    private StoreRepository storeRepository;

    public BasketSteps(BasketRepository basketRepository, StoreRepository storeRepository) {
        this.basketRepository = basketRepository;
        this.storeRepository = storeRepository;
    }

    @Lorsque("^des paniers existent:$")
    public void desPaniersExistent(DataTable dataTable) {
        List<Map<String, String>> dataMaps = dataTable.asMaps(String.class, String.class);
        dataMaps.forEach(dataMap -> {
            User user = User.builder().name(dataMap.get("user")).build();
            Set<Good> goods = new LinkedHashSet<>();
            if (!dataMap.get("quantities").equals("")) {
                List<String> quantities = Arrays.stream(dataMap.get("quantities").split(",")).collect(Collectors.toList());
                List<String> products = Arrays.stream(dataMap.get("products").split(",")).collect(Collectors.toList());
                List<String> stores = Arrays.stream(dataMap.get("provenances").split(",")).collect(Collectors.toList());
                goods = IntStream.range(0, quantities.size()).mapToObj(index -> {
                    Product product = Product.builder().name(products.get(index)).build();
                    Quantity quantity = Quantity.builder().value(Integer.parseInt(quantities.get(index))).build();
                    val store = storeRepository.find(stores.get(index)).get();
                    return Good.builder().product(product).quantity(quantity).maybeOrigin(ofNullable(store)).build();
                }).collect(Collectors.toSet());
            }
            Basket basket = Basket.builder().name(dataMap.get("basket")).user(user).goods(goods).build();
            basketRepository.save(basket);
        });
    }


    @Alors("^le panier \"([^\"]*)\" contient \"([^\"]*)\" \"([^\"]*)\" en provenance de \"([^\"]*)\"$")
    public void lePanierContientEnProvenanceDe(String basketName, String quantityValue, String productName, String storeName) {
        val maybeBasket = basketRepository.find(basketName);
        assertThat(maybeBasket).isNotEmpty();

        val maybeStore = storeRepository.find(storeName);
        assertThat(maybeStore).isNotEmpty();

        Good good = maybeBasket.get()
                .getGoods()
                .stream()
                .filter(it -> it.getProduct().getName().equals(productName))
                .filter(it -> it.getMaybeOrigin().equals(maybeStore))
                .findFirst().orElse(Good.builder().build());

        assertThat(good.getQuantity().getValue()).isEqualTo(Integer.parseInt(quantityValue));
    }

    @Et("^le panier \"([^\"]*)\" contient \"([^\"]*)\" \"([^\"]*)\"$")
    public void lePanierContient(String basketName, String quantityValue, String productName) throws Throwable {
        val maybeBasket = basketRepository.find(basketName);
        assertThat(maybeBasket).isNotEmpty();

        val quantity = maybeBasket.get()
                .getGoods()
                .stream()
                .filter(it -> it.getProduct().getName().equals(productName))
                .map(Good::getQuantity)
                .reduce(Quantity::plus)
                .orElse(Quantity.builder().build());

        assertThat(quantity.getValue()).isEqualTo(Integer.parseInt(quantityValue));
    }
}
