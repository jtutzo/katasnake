package com.jtutzo.katasnake.domain.services;

import com.jtutzo.katasnake.domain.gateways.CurrentUserGateway;
import com.jtutzo.katasnake.domain.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class UserServiceTest {

    private UserService userService;

    @Mock
    private CurrentUserGateway currentUserGateway;

    private User user = User.builder().name("Jeremy").build();

    @BeforeEach
    void setUp() {
        initMocks(this);
        userService = new UserService(currentUserGateway);
    }

    @Test
    void should_return_current_user() {
        // Given
        when(currentUserGateway.get()).thenReturn(Optional.of(user));

        // When
        Optional<User> maybeUser = userService.getCurrentUser();

        // Then
        assertThat(maybeUser).isNotEmpty();
        assertThat(maybeUser.get()).isEqualTo(user);
        verify(currentUserGateway).get();
    }

}
