package com.jtutzo.katasnake.domain.services;

import com.jtutzo.katasnake.domain.model.*;
import com.jtutzo.katasnake.domain.model.usecases.ExchangeGood;
import com.jtutzo.katasnake.domain.repositories.BasketRepository;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;


class BasketServiceTest {

    private BasketService basketService;

    @Mock
    private BasketRepository basketRepository;

    @Mock
    private UserService userService;

    @Mock
    private ExchangeGood exchangeGood;

    @Mock
    private StoreService storeService;

    private User user = User.builder().name("jeremy.tutzo").build();

    private Basket basket = Basket.builder().name("My basket").user(user).build();

    private Store store = Store.builder().name("Main store").build();

    @BeforeEach
    void setUp() {
        initMocks(this);
        basketService = new BasketService(basketRepository, userService, storeService, exchangeGood);
    }

    @Nested
    @DisplayName("When create a new basket")
    class CreateNewBasket {

        @BeforeEach
        public void setUp() {
            when(userService.getCurrentUser()).thenReturn(of(user));
        }

        @Test
        @DisplayName("is created with empty stock")
        void isCreatedWithEmptyStore() {
            // When
            val expected = basketService.create(basket.getName());

            // Then
            verify(basketRepository).save(basket);
            assertThat(expected).isEqualTo(basket);
            assertThat(expected.getUser()).isEqualTo(user);
            assertThat(expected.getGoods()).isEmpty();
        }

        @Test
        @DisplayName("throws exception when no current user")
        void throwWhenNoCurrentUser() {
            // Given
            when(userService.getCurrentUser()).thenReturn(empty());

            // When
            val exception = assertThatThrownBy(() -> basketService.create(basket.getName()));

            // Then
            exception.hasMessage("Current user not found !");
            verify(basketRepository, never()).save(basket);
        }
    }

    @Nested
    @DisplayName(("When delete a basket"))
    class DeleteBasket {

        @BeforeEach
        public void setUp() {
            when(userService.getCurrentUser()).thenReturn(of(user));
            basketService.create(basket.getName());
        }

        @Test
        @DisplayName("is deleted")
        void isDeleted() {
            val basketName = basket.getName();

            basketService.delete(basketName);

            verify(basketRepository).delete(basketName);
        }
    }

    @Nested
    @DisplayName("When add good in basket")
    class AddGoodInBasket {

        private Good good;

        @BeforeEach
        void setUp() {
            val product = Product.builder().name("IPhone").build();
            val quantity = Quantity.builder().value(1).build();
            good = Good.builder().product(product).quantity(quantity).maybeOrigin(of(store)).build();
            when(userService.getCurrentUser()).thenReturn(of(user));
            when(basketService.find(basket.getName())).thenReturn(of(basket));
            when(storeService.find(store.getName())).thenReturn(of(store));
        }

        @Test
        @DisplayName("is added")
        void isAdded() {
            // When
            basketService.addGood(basket.getName(), good);

            // Then
            verify(exchangeGood).fromStoreToBasket(user, store, basket, good);
            verify(basketRepository).save(basket);
            verify(storeService).save(store);
        }

        @Test
        @DisplayName("throws exception when no current user")
        void throwWhenNoCurrentUser() {
            // Given
            when(userService.getCurrentUser()).thenReturn(empty());

            // When
            val exception = assertThatThrownBy(() -> basketService.addGood(basket.getName(), good));

            // Then
            exception.hasMessage("Current user not found !");
            verify(exchangeGood, never()).fromStoreToBasket(user, store, basket, good);
            verify(basketRepository, never()).save(basket);
            verify(storeService, never()).save(store);
        }

        @Test
        @DisplayName("throw exception when store doesn't exist")
        void throwWhenStoreDoesNotExist() {
            // Given
            when(storeService.find(store.getName())).thenReturn(empty());

            // When
            val exception = assertThatThrownBy(() -> basketService.addGood(basket.getName(), good));

            // Then
            exception.hasMessage("Store " + store.getName() + "not found !");
            verify(exchangeGood, never()).fromStoreToBasket(user, store, basket, good);
            verify(basketRepository, never()).save(basket);
            verify(storeService, never()).save(store);
        }

        @Test
        @DisplayName("throw exception when basket doesn't exist")
        void throwWhenBasketDoesNotExist() {
            // Given
            when(basketService.find(basket.getName())).thenReturn(empty());

            // When
            val exception = assertThatThrownBy(() -> basketService.addGood(basket.getName(), good));

            // Then
            exception.hasMessage("Basket " + basket.getName() + "not found !");
            verify(exchangeGood, never()).fromStoreToBasket(user, store, basket, good);
            verify(basketRepository, never()).save(basket);
            verify(storeService, never()).save(store);
        }
    }

    @Nested
    @DisplayName("When read on basket")
    class ReadOnBasket {

        @Test
        @DisplayName("Should find all")
        void shouldFindAll() {
            // Given
            when(basketRepository.findAll()).thenReturn(Stream.of(basket).collect(Collectors.toSet()));

            // When
            Set<Basket> baskets = basketService.findAll();

            // Then
            verify(basketRepository).findAll();
            assertThat(baskets).containsExactly(basket);
        }

        @Test
        @DisplayName("Should find by name")
        void shouldFindByName() {
            // Given
            when(basketRepository.find(basket.getName())).thenReturn(of(basket));

            // When
            Optional<Basket> maybeBasket = basketService.find(basket.getName());

            // Then
            verify(basketRepository).find(basket.getName());
            assertThat(maybeBasket).isNotEmpty();
            assertThat(maybeBasket.get()).isEqualTo(basket);
        }
    }

}
