package com.jtutzo.katasnake.domain.services;

import com.jtutzo.katasnake.domain.model.Good;
import com.jtutzo.katasnake.domain.model.Product;
import com.jtutzo.katasnake.domain.model.Quantity;
import com.jtutzo.katasnake.domain.model.Store;
import com.jtutzo.katasnake.domain.repositories.StoreRepository;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class StoreServiceTest {

    private StoreService storeService;

    @Mock
    private StoreRepository storeRepository;

    @Mock
    private Store store = Store.builder().name("Main store").build();

    @BeforeEach
    void setUp() {
        initMocks(this);
        storeService = new StoreService(storeRepository);
    }

    @Nested
    @DisplayName("When create a new store")
    class CreateNewStore {

        @Test
        @DisplayName("is created with empty stock")
        void isCreatedWithEmptyStore() {
            String storeName = store.getName();

            Store store = storeService.create(storeName);

            verify(storeRepository).save(store);
            assertThat(store.getName()).isEqualTo(storeName);
            assertThat(store.getGoods()).isEmpty();
        }
    }

    @Nested
    @DisplayName("When save a store")
    class SaveAStore {

        @Test
        @DisplayName("is saved")
        void isSaved() {
            storeService.save(store);

            verify(storeRepository).save(store);
        }
    }

    @Nested
    @DisplayName(("When delete a store"))
    class DeleteStore {

        @BeforeEach
        void setUp() {
            storeService.create(store.getName());
        }

        @Test
        @DisplayName("is deleted")
        void isDeleted() {
            val name = store.getName();

            storeService.delete(name);

            verify(storeRepository).delete(name);
        }
    }

    @Nested
    @DisplayName(("When add good in store"))
    class addGoodInStore {

        @BeforeEach
        void setUp() {
            when(storeService.find(store.getName())).thenReturn(of(store));
        }

        @Test
        @DisplayName("is added")
        void isAdded() {
            // Given
            Product product = Product.builder().name("IPhone").build();
            Quantity quantity = Quantity.builder().value(1).build();
            Good good = Good.builder().product(product).quantity(quantity).build();

            // Then
            storeService.addGood(store.getName(), good);

            // When
            verify(store).add(good);
            verify(storeRepository).save(store);
        }

        @Test
        @DisplayName("throw exception when store doesn't exist")
        void throwWhenStoreDoesNotExist() {
            // Given
            Product product = Product.builder().name("IPhone").build();
            Quantity quantity = Quantity.builder().value(1).build();
            Good good = Good.builder().product(product).quantity(quantity).build();
            when(storeService.find(store.getName())).thenReturn(empty());

            // When
            val exception = assertThatThrownBy(() -> storeService.addGood(store.getName(), good));

            // Then
            exception.hasMessage("Store " + store.getName() + "not found !");
            verify(store, never()).add(good);
        }
    }

    @Nested
    @DisplayName("When read")
    class ReadInStore {

        @Test
        @DisplayName("Should find by name")
        void shouldFindByName() {
            // Given
            when(storeRepository.find(store.getName())).thenReturn(of(store));

            // When
            Optional<Store> maybeStore = storeService.find(store.getName());

            // Then
            verify(storeRepository).find(store.getName());
            assertThat(maybeStore).isNotEmpty();
            assertThat(maybeStore.get()).isEqualTo(store);
        }

        @Test
        @DisplayName("Should find all")
        void shouldFindAll() {
            // Given
            when(storeRepository.findAll()).thenReturn(Stream.of(store).collect(Collectors.toSet()));

            // When
            Set<Store> stores = storeService.findAll();

            // Then
            verify(storeRepository).findAll();
            assertThat(stores).containsExactly(store);
        }
    }

}
