package com.jtutzo.katasnake.resources;

import com.jtutzo.katasnake.resources.dto.AddGood;
import com.jtutzo.katasnake.resources.dto.CreateStore;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@QuarkusTest
public class StoreResourceTest {
    public static String URL = "/stores";
    private CreateStore createStore = CreateStore.builder().name("my-store").build();
    private AddGood addGood = AddGood.builder().product("T-shirt").quantity(2).build();

    @AfterEach
    public void reset() {
        given().pathParam("name", createStore.getName())
                .delete(URL + "/{name}");
    }

    @Test
    @DisplayName("Should return 201 when create my-store entity")
    public void create() {
        given()
                .contentType(ContentType.JSON)
                .body(createStore)
                .when()
                .post(URL)
                .then()
                .statusCode(201)
                .contentType(ContentType.JSON)
                .body(is("{\"name\":\"my-store\",\"goods\":[]}"));
    }

    @Test
    @DisplayName("Should return 200 when add good")
    public void addGood() {
        given().contentType(ContentType.JSON).body(createStore).post(URL);
        given()
                .contentType(ContentType.JSON)
                .body(addGood)
                .pathParam("name", createStore.getName())
                .when()
                .put(URL + "/{name}/add")
                .then()
                .statusCode(200);
    }

    @Test
    @DisplayName("Should return list include my-store entity and 200 http response when find all")
    public void findAll() {
        given().contentType(ContentType.JSON).body(createStore).post(URL);

        given()
                .when()
                .get(URL)
                .then()
                .statusCode(200)
                .body(is("[{\"name\":\"my-store\",\"goods\":[]}]"));
    }

    @Test
    @DisplayName("Should return empty list entity and 200 http response when find all")
    public void findAllEmpty() {
        given()
                .when()
                .get(URL)
                .then()
                .statusCode(200)
                .body(is("[]"));
    }

    @Test
    @DisplayName("Should return my-store entity and 200 http response when find by name")
    public void findMyBasket() {
        given().contentType(ContentType.JSON).body(createStore).post(URL);
        given()
                .pathParam("name", "my-store")
                .when()
                .get(URL + "/{name}")
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body(is("{\"name\":\"my-store\",\"goods\":[]}"));
    }

    @Test
    @DisplayName("Should return nothing entity and 404 http response when find by name")
    public void findNothing() {
        given()
                .pathParam("name", "my-store")
                .when()
                .get(URL + "/{name}")
                .then()
                .statusCode(404)
                .body(is(""));
    }

    @Test
    @DisplayName("Should return 200 http response when delete by name")
    public void delete() {
        given().contentType(ContentType.JSON).body(createStore).post(URL);
        given()
                .pathParam("name", createStore.getName())
                .when()
                .delete(URL + "/{name}")
                .then()
                .statusCode(200);
    }
}
