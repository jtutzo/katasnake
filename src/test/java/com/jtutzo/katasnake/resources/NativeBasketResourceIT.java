package com.jtutzo.katasnake.resources;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeBasketResourceIT extends BasketResourceTest {

    // Execute the same tests but in native mode.
}