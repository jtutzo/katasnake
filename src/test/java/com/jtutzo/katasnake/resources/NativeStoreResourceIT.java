package com.jtutzo.katasnake.resources;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeStoreResourceIT extends StoreResourceTest {

    // Execute the same tests but in native mode.
}
