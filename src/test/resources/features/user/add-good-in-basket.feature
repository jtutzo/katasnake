# language: fr
Fonctionnalité: Tout utilisateur - ajouter un bien dans son panier

  L'utilisateur veut prendre un bien du magasin pour l'ajouter dans son panier
  Nous considerons dans les cas suivants que l'association d'une quantité et d'un produit s'appelle un bien

  Contexte:
    Etant donné des magasins existent:
      | magasin  | quantities | products              |
      | magasin1 | 12,15,12   | T-shirt,Pull,Lunettes |
      | magasin2 |            |                       |
      | magasin3 | 230        | Lunettes              |
      | magasin4 | 5,24       | Lunettes,Pull         |

    Etant donné des paniers existent:
      | basket  | user   | quantities | products              | provenances                |
      | panier1 | Mark   | 1,2,3      | T-shirt,Pull,Lunettes | magasin1,magasin1,magasin2 |
      | panier2 | Jean   |            |                       |                            |
      | panier3 | Michel | 8,4        | Lunettes,T-Shirt      | magasin2,magasin2          |


  Plan du Scénario: Le bien est disponible dans le magasin
    Quand "<utilisateur>" ajoute "<quantite>" "<produit>" au panier "<panier>" de provenance de "<magasin>"
    Alors Aucune erreur est levée
    Et le panier "<panier>" contient "<quantite_panier>" "<produit>" en provenance de "<magasin>"
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Exemples:
      | magasin  | panier  | utilisateur | produit  | quantite | quantite_panier | quantite_magasin |
      | magasin1 | panier1 | Mark        | T-shirt  | 3        | 4               | 9                |
      | magasin3 | panier2 | Jean        | Lunettes | 12       | 12              | 218              |

  Plan du Scénario: Le bien n'est pas disponible dans le magasin
    Quand "<utilisateur>" ajoute "<quantite>" "<produit>" au panier "<panier>" de provenance de "<magasin>"
    Alors l'erreur "<erreur>" est levée
    Et le panier "<panier>" contient "<quantite_panier>" "<produit>" en provenance de "<magasin>"
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Exemples:
      | magasin  | panier  | utilisateur | produit  | quantite | quantite_panier | quantite_magasin | erreur                                     |
      | magasin3 | panier1 | Mark        | T-shirt  | 3        | 0               | 0                | Le produit T-shirt n'est pas disponible !  |
      | magasin2 | panier3 | Michel      | Lunettes | 12       | 8               | 0                | Le produit Lunettes n'est pas disponible ! |

  Plan du Scénario: Ce n'est pas mon panier
    Quand "<utilisateur>" ajoute "<quantite>" "<produit>" au panier "<panier>" de provenance de "<magasin>"
    Alors l'erreur "<erreur>" est levée
    Et le panier "<panier>" contient "<quantite_panier>" "<produit>" en provenance de "<magasin>"
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Exemples:
      | magasin  | panier  | utilisateur | produit  | quantite | quantite_panier | quantite_magasin | erreur                                       |
      | magasin1 | panier1 | Jean        | T-shirt  | 3        | 1               | 12               | Vous pouvez modifier seulement vos paniers ! |
      | magasin3 | panier3 | Mark        | Lunettes | 12       | 0               | 230              | Vous pouvez modifier seulement vos paniers ! |

  Plan du Scénario: J'ajoute une quantité de produit plus importante que contenu dans le magasin
    Quand "<utilisateur>" ajoute "<quantite>" "<produit>" au panier "<panier>" de provenance de "<magasin>"
    Alors l'erreur "<erreur>" est levée
    Et le panier "<panier>" contient "<quantite_panier>" "<produit>" en provenance de "<magasin>"
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Exemples:
      | magasin  | panier  | utilisateur | produit  | quantite | quantite_panier | quantite_magasin | erreur                                                      |
      | magasin1 | panier1 | Mark        | T-shirt  | 13       | 1               | 12               | Quantité disponible insuffisante pour le produit T-shirt !  |
      | magasin3 | panier3 | Michel      | Lunettes | 500      | 0               | 230              | Quantité disponible insuffisante pour le produit Lunettes ! |

  Plan du Scénario: La provenance des biens est différentes
    Quand "<utilisateur>" ajoute "<quantite>" "<produit>" au panier "<panier>" de provenance de "<magasin>"
    Et "<utilisateur>" ajoute "<quantite_bis>" "<produit>" au panier "<panier>" de provenance de "<magasin_bis>"
    Alors Aucune erreur est levée
    Et le panier "<panier>" contient "<quantite_panier>" "<produit>" en provenance de "<magasin>"
    Et le panier "<panier>" contient "<quantite_panier_bis>" "<produit>" en provenance de "<magasin_bis>"
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Et le magasin "<magasin_bis>" contient "<quantite_magasin_bis>" "<produit>"
    Exemples:
      | magasin  | magasin_bis  | panier  | utilisateur | produit  | quantite | quantite_bis | quantite_panier | quantite_panier_bis | quantite_magasin | quantite_magasin_bis |
      | magasin1 | magasin4     | panier1 | Mark        | Pull     | 6        | 12           | 8               | 12                  | 9                | 12                   |
      | magasin3 | magasin1     | panier3 | Michel      | Lunettes | 150      | 1            | 150             | 1                   | 80               | 11                   |
