# language: fr
Fonctionnalité: Tout utilisateur - supprimer un bien de son panier

  L'utilisateur veut supprimer de son panier un bien et le redonner au magasin correspondant
  Nous considerons dans les cas suivants que l'association d'une quantité et d'un produit s'appelle un bien

  Contexte:
    Etant donné des magasins existent:
      | magasin  | quantities | products              |
      | magasin1 | 12,15      | T-shirt,Pull          |
      | magasin2 |            |                       |
      | magasin3 | 230        | Lunettes              |
      | magasin4 | 15,3,8     | Lunettes,T-shirt,Pull |

    Étant donné des paniers existent:
      | basket  | user   | quantities | products              | provenances                |
      | panier1 | Mark   | 1,2,3      | T-shirt,Pull,Lunettes | magasin1,magasin1,magasin2 |
      | panier2 | Jean   |            |                       |                            |
      | panier3 | Michel | 8,4        | Lunettes,T-Shirt      | magasin3,magasin2          |

  Plan du Scénario: Le bien est dans mon panier
    Quand "<utilisateur>" supprime "<quantite>" "<produit>" du panier "<panier>" de provenance de "<magasin>"
    Alors Aucune erreur est levée
    Et le panier "<panier>" contient "<quantite_panier>" "<produit>" en provenance de "<magasin>"
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Exemples:
      | magasin  | panier  | utilisateur | produit  | quantite | quantite_panier | quantite_magasin |
      | magasin1 | panier1 | Mark        | Pull     | 2        | 0               | 17               |
      | magasin3 | panier3 | Michel      | Lunettes | 6        | 2               | 236              |

  Plan du Scénario: Le bien n'est pas dans mon panier
    Quand "<utilisateur>" supprime "<quantite>" "<produit>" du panier "<panier>" de provenance de "<magasin>"
    Alors l'erreur "<erreur>" est levée
    Et le panier "<panier>" contient "<quantite_panier>" "<produit>" en provenance de "<magasin>"
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Exemples:
      | magasin  | panier  | utilisateur | produit | quantite | quantite_panier | quantite_magasin | erreur                                    |
      | magasin3 | panier2 | Jean        | T-shirt | 3        | 0               | 0                | Le produit T-shirt n'est pas disponible ! |
      | magasin1 | panier3 | Michel      | Pull    | 12       | 0               | 15               | Le produit Pull n'est pas disponible !    |

  Plan du Scénario: Ce n'est pas mon panier
    Quand "<utilisateur>" supprime "<quantite>" "<produit>" du panier "<panier>" de provenance de "<magasin>"
    Alors l'erreur "<erreur>" est levée
    Et le panier "<panier>" contient "<quantite_panier>" "<produit>" en provenance de "<magasin>"
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Exemples:
      | magasin  | panier  | utilisateur | produit  | quantite | quantite_panier | quantite_magasin | erreur                                       |
      | magasin1 | panier1 | Jean        | Pull     | 2        | 2               | 15               | Vous pouvez modifier seulement vos paniers ! |
      | magasin3 | panier3 | Mark        | Lunettes | 6        | 8               | 230              | Vous pouvez modifier seulement vos paniers ! |

  Plan du Scénario: Je supprime une quantité de produit plus importante que contenu dans mon panier
    Quand "<utilisateur>" supprime "<quantite>" "<produit>" du panier "<panier>" de provenance de "<magasin>"
    Alors l'erreur "<erreur>" est levée
    Et le panier "<panier>" contient "<quantite_panier>" "<produit>" en provenance de "<magasin>"
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Exemples:
      | magasin  | panier  | utilisateur | produit  | quantite | quantite_panier | quantite_magasin | erreur                                                      |
      | magasin1 | panier1 | Mark        | T-shirt  | 13       | 1               | 12               | Quantité disponible insuffisante pour le produit T-shirt !  |
      | magasin3 | panier3 | Michel      | Lunettes | 500      | 8               | 230              | Quantité disponible insuffisante pour le produit Lunettes ! |

  Plan du Scénario: Je supprime un bien de provenance différente
    Et "<utilisateur>" supprime "<quantite>" "<produit>" du panier "<panier>" de provenance de "<magasin2>"
    Alors l'erreur "<erreur>" est levée
    Et le panier "<panier>" contient "<quantite_panier>" "<produit>" en provenance de "<magasin>"
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Et le magasin "<magasin2>" contient "<quantite_magasin2>" "<produit>"
    Exemples:
      | magasin  | magasin2 | panier  | utilisateur | produit  | quantite | quantite_panier | quantite_magasin | quantite_magasin2 | erreur                          |
      | magasin1 | magasin2 | panier1 | Mark        | Pull     | 2        | 2               | 15               | 0                 | L'origine du bien est incorecte |
      | magasin3 | magasin4 | panier3 | Michel      | Lunettes | 6        | 8               | 230              | 15                | L'origine du bien est incorecte |
