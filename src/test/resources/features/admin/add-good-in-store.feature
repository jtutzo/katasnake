# language: fr
Fonctionnalité: Administrateur - ajouter un bien dans un magasin

  L'administrateur veut ajouter un bien dans un magasin
  Nous considerons dans les cas suivants que l'association d'une quantité et d'un produit s'appelle un bien

  Contexte:
    Etant donné des magasins existent:
      | magasin  | quantities | products              |
      | magasin1 | 12,15,12   | T-shirt,Pull,Lunettes |
      | magasin2 | 230        | Lunettes              |

  Plan du Scénario: Le bien est ajouter dans le magasin
    Quand l'"<administrateur>" ajoute "<quantite>" "<produit>" dans le "<magasin>"
    Alors Aucune erreur est levée
    Et le magasin "<magasin>" contient "<quantite_magasin>" "<produit>"
    Exemples:
      | magasin  | administrateur | produit  | quantite | quantite_magasin |
      | magasin1 | Mark           | T-shirt  | 3        | 15               |
      | magasin2 | Jean           | Lunettes | 12       | 242              |
