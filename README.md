# Katasnake
[![build-status](https://img.shields.io/bitbucket/pipelines/jtutzo/katasnake.svg)](https://bitbucket.org/jtutzo/katasnake/addon/pipelines/home#!/)

## Run test
```bash
mvn test
```

## Run dev
```bash
# Start mongo database on localhost
docker run --rm -p 27017:27017 --name mongo -d mongo

# Start app on dev
mvn compile quarkus:dev
```

## Build native
```bash
mvn clean package -Pnative
```
