#!/usr/bin/env bash

set -o errexit
set -o nounset

IMAGE_VERSION=1.0-SNAPSHOT
IMAGE_GROUP=jtutzo
IMAGE_NAME=katasnake-jvm-dev
export APP_NAME=$IMAGE_NAME

echo "APP_NAME=$APP_NAME"
echo "IMAGE_GROUP=$IMAGE_GROUP"
echo "IMAGE_NAME=$IMAGE_NAME"
echo "IMAGE_VERSION=$IMAGE_VERSION"

echo "Build app"
mvn clean package

echo "Build and push image"
docker build -f src/main/docker/Dockerfile.jvm -t $IMAGE_GROUP/$IMAGE_NAME:$IMAGE_VERSION .
docker push $IMAGE_GROUP/$IMAGE_NAME:$IMAGE_VERSION

echo "Deploy on local cluster"
kubectl apply -f target/kubernetes/kubernetes.yml
